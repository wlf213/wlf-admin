// 全局基础类型
declare namespace WJ {
  /* 菜单项 */
  interface MenuItem {
    // 菜单ID不可重复!
    id: string;
    // 上级菜单ID，无上级时赋'0'
    pid: string;
    // 菜单名称
    name: string;
    // 菜单路径,支持路由和全路径url，如/index或https://www.baidu.com
    path: string;
    // 是否隐藏,true隐藏
    isHide?: boolean;
    // 菜单排序，数字越大越靠后
    sort: number;
    // 菜单权限
    auth?: string;
    // 菜单图标
    icon?: string;
    // 是否新页面打开链接，
    isBlank?: boolean;
    // 构建子菜单用
    children?: Array<MenuItem>;
  }


}

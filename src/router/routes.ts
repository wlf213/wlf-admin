import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/login',
    component: () => import('pages/LoginPage.vue'),
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/IndexPage.vue') },
      { path: '/index', component: () => import('pages/IndexPage2.vue') },
      { path: '/sys-user', component: () => import('pages/UserPage.vue') },
      { path: '/sys-menu', component: () => import('pages/MenuPage.vue') },
      { path: '/sys-role', component: () => import('pages/RolePage.vue') },
      {
        path: '/outside-link',
        component: () => import('pages/OutsidePage.vue'),
        props: (route) => ({ url: route.query.url }),
      },
      { path: '/comp/table', component: () => import('pages/TablePage.vue') },
      { path: '/comp/card', component: () => import('pages/CardPage.vue') },
      { path: '/comp/chart', component: () => import('pages/ChartsPage.vue') },
      {
        path: '/page-filter',
        component: () => import('pages/PaginationPage.vue'),
      },
      { path: '/comp/form', component: () => import('pages/FormPage.vue') },
      {
        path: '/comp/contact',
        component: () => import('pages/ContactPage.vue'),
      },
      {
        path: '/comp/directory',
        component: () => import('pages/DirectoryPage.vue'),
      },
      { path: '/comp/footer', component: () => import('pages/FooterPage.vue') },
      {
        path: '/comp/product',
        component: () => import('pages/ProductCatalogues.vue'),
      },
      {
        path: '/comp/treetable',
        component: () => import('pages/TreeTable.vue'),
      },
      {
        path: '/comp/userprofile',
        component: () => import('pages/UserProfile.vue'),
      },
      { path: '/sys/i18n', component: () => import('pages/I18nPage.vue') },
      { path: '/sys/crud', component: () => import('pages/CrudPage.vue') },
    ],
  },
  {
    path: '/main',
    component: () => import('pages/IndexPage2.vue'),
  },
  {
    path: '/login-page',
    component: () => import('pages/Login-1.vue'),
  },
  {
    path: '/locking',
    component: () => import('pages/LockScreen.vue'),
  },

  {
    path: '/locking2',
    component: () => import('pages/LockScreen-2.vue'),
  },

  {
    path: '/maintenance',
    component: () => import('pages/MaintenancePage.vue'),
  },
  {
    path: '/pricing',
    component: () => import('pages/PricingPage.vue'),
  },
  {
    path: '/mail',
    component: () => import('layouts/MailLayout.vue'),
  },
  {
    path: '/search',
    component: () => import('layouts/SearchLayout.vue'),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;

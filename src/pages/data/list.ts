
export const userList = [{ id: '0', name: '用户0', roleName: '超级管理员0', typeName: '类别0', status: '禁用0', age: 0, loginNo: '登录帐号0', },
{ id: '1', name: '用户1', roleName: '超级管理员1', typeName: '类别1', status: '禁用1', age: 1, loginNo: '登录帐号1', },
{ id: '2', name: '用户2', roleName: '超级管理员2', typeName: '类别2', status: '禁用2', age: 2, loginNo: '登录帐号2', },
{ id: '3', name: '用户3', roleName: '超级管理员3', typeName: '类别3', status: '禁用3', age: 3, loginNo: '登录帐号3', },
{ id: '4', name: '用户4', roleName: '超级管理员4', typeName: '类别4', status: '禁用4', age: 4, loginNo: '登录帐号4', },
{ id: '5', name: '用户5', roleName: '超级管理员5', typeName: '类别5', status: '禁用5', age: 5, loginNo: '登录帐号5', },
{ id: '6', name: '用户6', roleName: '超级管理员6', typeName: '类别6', status: '禁用6', age: 6, loginNo: '登录帐号6', },
{ id: '7', name: '用户7', roleName: '超级管理员7', typeName: '类别7', status: '禁用7', age: 7, loginNo: '登录帐号7', },
{ id: '8', name: '用户8', roleName: '超级管理员8', typeName: '类别8', status: '禁用8', age: 8, loginNo: '登录帐号8', },
{ id: '9', name: '用户9', roleName: '超级管理员9', typeName: '类别9', status: '禁用9', age: 9, loginNo: '登录帐号9', },
{ id: '10', name: '用户10', roleName: '超级管理员10', typeName: '类别10', status: '禁用10', age: 10, loginNo: '登录帐号10', },
{ id: '11', name: '用户11', roleName: '超级管理员11', typeName: '类别11', status: '禁用11', age: 11, loginNo: '登录帐号11', },
{ id: '12', name: '用户12', roleName: '超级管理员12', typeName: '类别12', status: '禁用12', age: 12, loginNo: '登录帐号12', },
{ id: '13', name: '用户13', roleName: '超级管理员13', typeName: '类别13', status: '禁用13', age: 13, loginNo: '登录帐号13', },
{ id: '14', name: '用户14', roleName: '超级管理员14', typeName: '类别14', status: '禁用14', age: 14, loginNo: '登录帐号14', },
{ id: '15', name: '用户15', roleName: '超级管理员15', typeName: '类别15', status: '禁用15', age: 15, loginNo: '登录帐号15', },
{ id: '16', name: '用户16', roleName: '超级管理员16', typeName: '类别16', status: '禁用16', age: 16, loginNo: '登录帐号16', },
{ id: '17', name: '用户17', roleName: '超级管理员17', typeName: '类别17', status: '禁用17', age: 17, loginNo: '登录帐号17', },
{ id: '18', name: '用户18', roleName: '超级管理员18', typeName: '类别18', status: '禁用18', age: 18, loginNo: '登录帐号18', },
{ id: '19', name: '用户19', roleName: '超级管理员19', typeName: '类别19', status: '禁用19', age: 19, loginNo: '登录帐号19', },
{ id: '20', name: '用户20', roleName: '超级管理员20', typeName: '类别20', status: '禁用20', age: 20, loginNo: '登录帐号20', },
{ id: '21', name: '用户21', roleName: '超级管理员21', typeName: '类别21', status: '禁用21', age: 21, loginNo: '登录帐号21', },
{ id: '22', name: '用户22', roleName: '超级管理员22', typeName: '类别22', status: '禁用22', age: 22, loginNo: '登录帐号22', },
];


export const roleList = [
  { id: '1', name: '超级管理员', code: 'admin', reamrk: '超级管理员' },
  { id: '2', name: '测试管理员', code: 'tesr', reamrk: '测试管理员' },
]

export const baseImg =
  'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fsafe-img.xhscdn.com%2Fbw1%2F4d2a8885-131d-4530-835a-0ee12ae4201b%3FimageView2%2F2%2Fw%2F1080%2Fformat%2Fjpg&refer=http%3A%2F%2Fsafe-img.xhscdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1686664782&t=8789789109da9319243a2ea7ffe4d1fe';
export const baseImg2 =
  'https://img2.baidu.com/it/u=473659940,2616707866&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500';

export const baseImg3 =
  'https://img0.baidu.com/it/u=2837801980,2735196303&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500';


export const messages = [
  {
    id: 5,
    name: '小红',
    msg: ' 如果小狗',
    avatar: baseImg2,
    time: '10:42 PM',
  },
  {
    id: 6,
    name: '小白',
    msg: ' 如果小狗',
    avatar: baseImg2,
    time: '11:17 AM',
  },
  {
    id: 1,
    name: '孩子',
    msg: ' 如果小狗',
    avatar: baseImg2,
    time: '5:17 AM',
  },
  {
    id: 2,
    name: '小杰',
    msg: ' 如果小狗',
    avatar: baseImg2,
    time: '5:17 AM',
  },
  {
    id: 3,
    name: '小红',
    msg: ' 如果小狗',
    avatar: baseImg2,
    time: '5:17 AM',
  },
];
export const contacts = [
  {
    name: '小P',
    position: '开发',
    avatar: baseImg2,
  },
  {
    name: 'R先生',
    position: '开发',
    avatar: baseImg2,
  },
  {
    name: '小杰',
    position: '开发',
    avatar: baseImg2,
  },
  {
    name: 'B友',
    position: '管理员',
    avatar: baseImg2,
  },
  {
    name: '老哥',
    position: '管理员',
    avatar: baseImg2,
  },
];

export const sales_data = [
  {
    name: '小红',
    Progress: 70,
    status: '关闭',
    stock: '14 / 30',
    date: '2018年10月23日',
    avatar: baseImg,
    product_name: '包包',
    total: '300.00',
    code: 'QWE123',
    prod_img: baseImg,
  },
  {
    name: '小黑',
    Progress: 60,
    status: '发送',
    date: '2018年11月11日',
    stock: '25 / 70',
    avatar: baseImg,
    product_name: '电脑',
    total: '230,00',
    code: 'ABC890',
    prod_img: baseImg,
  },
  {
    name: '小刘',
    Progress: 30,
    status: '等待',
    stock: '35 / 50',
    avatar: baseImg,
    product_name: 'PS5',
    total: '34,00',
    date: '19 Sept 2020',
    code: 'GHI556',
    prod_img: baseImg,
  },
  {
    name: '小杰',
    Progress: 100,
    status: '已付费',
    stock: '18 / 33',
    avatar: baseImg,
    product_name: 'SW游戏机',
    total: '208,00',
    date: '2020年9月19日',
    code: 'JKL345',
    prod_img: baseImg,
  },
];

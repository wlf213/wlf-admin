import { defineStore } from 'pinia'
import { uid } from 'quasar'
import { useLayoutStore } from './layout-store'
import { nextTick } from 'vue'
import { useRouter } from 'vue-router'

/**
 * 一些需要全局用的方法挂载在这个上面
 * 页面中使用方法
 * const $w = useWlfStore()
 */
export const useWlfStore = defineStore('wlf', () => {

  const layout = useLayoutStore();
  const router = useRouter();
  /**
   * Get请求
   */
  const getData = () => {
    return {}
  }

  // 窗口操作，临时打开一个页面
  const openPage = (name: string, path: string) => {
    layout.menuList.push({
      id: uid(),
      pid: '0',
      name: name,
      path: path,
      sort: 0,
      isHide: true,
    })
    nextTick(() => {
      router.push(path)
    })
  }
  return { getData, openPage };
})

/* 用来写一些生成数据的函数 */
function main() {
  for (let index = 0; index < 23; index++) {
    console.log(
      "{ id: '" +
        index +
        "', name: '用户" +
        index +
        "', roleName: '超级管理员" +
        index +
        "', typeName: '类别" +
        index +
        "', status: '禁用" +
        index +
        "', age: " +
        index +
        ", loginNo: '登录帐号" +
        index +
        "', },"
    );
  }
}
main();

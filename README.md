基于Vue3+TS+Setup+Quasar写的一个前端后台管理框架。

# 项目诞生缘由

觉得[Quasar](https://www.quasar-cn.cn/)这个材料很精致，且有打算写一个前端框架。

已完成
- 主体布局
- 动态菜单+静态路由(后端开发者会非常熟悉这种动态构建菜单的方式)
- Tab栏
- 面包屑
- 切页动画，页面缓存，等一系列的动画，

# 演示地址

[演示地址](http://wangijun.com:8003/)

# 开发
开发本地需要quasar-cli,未安装需要安装,执行命令`npm i -g @quasar/cli`

- clone项目
- cd wlf-admin
- npm i
- `quasar dev` or `npm run dev`

# 部署
按传统前端项目编译打包即可。
- `quasar build`

# 项目展示

## 首页
![首页](./readme_img/indexPage.png);

## 菜单
![菜单](./readme_img/menuPage.png);
## 角色
![菜单](./readme_img/rolePage.png);
## 用户
![菜单](./readme_img/userPage.png);
